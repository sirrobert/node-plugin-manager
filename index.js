"use strict";

const path  = require('path');
const stack = require('callsite');
const fs    = require('fs');

class PluginManager {
	constructor (params={}) {

    var callerPath = stack()[1]
      .getFileName()
      .split('/')
      .slice(0,-1)
      .join('/') + '/';

    this.addPath(callerPath);

    if (params.paths) {
      this.addPaths(params.paths);
    }

    if (params.path) {
      this.addPath(params.path);
    }
	}

  // Suffixes are in order of priority.  The earlier in the array, the
  // higher the priority of the suffix.  Files with earlier suffixes will be
  // matched before those with later suffixes, even if both exist.
  get suffixes () {
    if (!this._suffixes) {
      this._suffixes = [
        '.plugin.js',
        '.plugin',
        '.js'
      ];
    }

    return this._suffixes;
  }

  // Add a suffix to the TOP of the suffixes list.
  addSuffix (suffix) {
    this.suffixes.unshift(suffix);
    return this;
  }

  // Send in arguments or an array or multiple arrays.  Examples:
  //  .addSuffixes(".foo");
  //  .addSuffixes(".foo",".bar",".baz")'
  //  .addSuffixes([".foo", ".bar"]);
  //  .addSuffixes(".foo", [".bar", ".baz"], [".bim"]);
  addSuffixes () {
    var args = Array.from(arguments).reverse();
    var suffixes = [];
    args.forEach(arg => {
      suffixes = suffixes.concat(arg);
    });

    suffixes.forEach(suffix => {
      this.addSuffix(suffix);
    });
    
    return this;
  }

  // Paths are in order of priority.  The earlier in the array, the higher
  // the priority of the path.  Files found in earlier paths will be matched
  // before those found in later paths, even if both exists.
  //
  // Paths are completely prior to suffixes.  This means that if a file with
  // even the lowest priority suffix is found in the first path, it will be
  // used in lieu of a file with a higher priority suffix in the second
  // path of the array.
  get paths () {
    if (!this._paths) {
      this._paths = [
        path.resolve("./"),
        path.resolve("./plugins")
      ];
    }

    return this._paths;
  }

  // Given a file name, return a prioritized list of paths (including
  // filename) that should be checked.
  fileplex (filename) {
    var results = [];
    for (var p = 0; p < this.paths.length; p++) {
      for (var s = 0; s < this.suffixes.length; s++) {
        var pattern = this.suffixes[s].replace('.','\\.');
        pattern = new RegExp(pattern + '$');
        var result = path.resolve(
          this.paths[p],
          filename + (filename.match(pattern) ? '' : this.suffixes[s])
        );
        results.push(result);
      }
    }
    return results;
  }

  // Add a path to the paths array.
  addPath () {
    var dir = path.resolve(...Array.from(arguments));

    if (typeof dir === "string") {
      this.paths.unshift(path.resolve(dir));
      return this;
    }

    throw new Error('Error adding ' + dir + '.  Cannot add non-string dirs.');
  }

  // Add multiple paths to the paths array.
  addPaths () {
    var dirs = [].concat(Array.from(arguments)).shift().reverse();
    dirs.forEach(
      dir => {this.addPath(dir)}
    );
    return this;
  }

  // A convenience alias for require.
  load (filename) {
    return this.require(filename);
  }

  // Require a plugin.
  require (filename) {
    
    if (!filename) {
      throw new Error("Cannot require a plugin with no filename provided.");
    }

    var selected = null;

    var fileplex = this.fileplex(filename);
    for (var f = 0; f < fileplex.length; f++) {
      if (fs.existsSync(fileplex[f])) {
        selected = fileplex[f];
        break;
      }
    }

    if (!selected) {
      return null;
    }

    // Ok, we have selected a real file on disk.  Let's load it up.
    var _module = new module.constructor();
    _module.paths = module.paths;

    var content = fs.readFileSync(selected).toString();

    _module.PluginManager = this;
    _module._compile(content, selected);

    return _module.exports;
  }

}

module.exports = PluginManager;
