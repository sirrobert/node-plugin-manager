"use strict";

const PluginManager = require('../index.js');
const expect        = require('unexpected');
const path          = require('path');

describe("class PluginManager", function () {

  describe("instance", function () {

    it("should have default paths", function () {
      var mgr = new PluginManager();
      expect(mgr.paths[0], "to be", path.resolve(__dirname));
      expect(mgr.paths[1], "to be", path.resolve('./'));
    });

    it("should take a 'path' property in construction", function () {
      var mgr = new PluginManager({path: __dirname + "/../plugins"});
      expect(mgr.paths[0], "to be", path.resolve(__dirname + "/../plugins"));
      expect(mgr.paths[1], "to be", path.resolve(__dirname + '/'));
      expect(mgr.paths[2], "to be", path.resolve('./'));
    });

    it("should take a 'paths' property in construction", function () {
      var mgr = new PluginManager({
        paths: [
          __dirname + "/../plugins",
          __dirname + "/../plugins/more-plugins"
        ]
      });
      
      expect(mgr.paths[0], "to be", path.resolve(__dirname + "/../plugins"));
      expect(mgr.paths[1], "to be", path.resolve(__dirname + "/../plugins/more-plugins"));
      expect(mgr.paths[2], "to be", path.resolve(__dirname + '/'));
      expect(mgr.paths[3], "to be", path.resolve('./'));
    });

    it("should load single-depth modules correctly", function () {
      var mgr = new PluginManager();
      mgr.addPath(__dirname, "..","tmp","plugins");
      mgr.addPath(__dirname, "..","tmp","plugins","more-plugins");

      const spanish = mgr.require('spanish.plugin.js');
      expect(spanish, "to be", "oye como va");
    });

    it("should load double-depth modules correctly", function () {
      var mgr = new PluginManager();
      mgr.addPath(__dirname, "..","tmp","plugins");
      mgr.addPath(__dirname, "..","tmp","plugins","more-plugins");

      var Polyglot = mgr.require('polyglot.plugin.js');
      var polyglot = new Polyglot();

      expect(polyglot.french  , "to be" , "c'est la vie");
      expect(polyglot.spanish , "to be" , "oye como va");
      expect(polyglot.latin   , "to be" , "lorem ipsum dolet amor");
    });

    it("should let users add suffixes", function () {
      var mgr = new PluginManager();
      mgr.addPath(__dirname, "..","tmp","plugins");
      mgr.addSuffix('-pluggy');

      expect(mgr.suffixes[0], "to be", "-pluggy");
      expect(mgr.suffixes[1], "to be", ".plugin.js");
      expect(mgr.suffixes[2], "to be", ".plugin");
      expect(mgr.suffixes[3], "to be", ".js");

      mgr.addSuffixes("-plork", ".plork.js", ".js");

      expect(mgr.suffixes[0] , "to be" , "-plork"    );
      expect(mgr.suffixes[1] , "to be" , ".plork.js" );
      expect(mgr.suffixes[2] , "to be" , ".js"       );
      expect(mgr.suffixes[3] , "to be" , "-pluggy"   );
      expect(mgr.suffixes[4] , "to be" , ".plugin.js");
      expect(mgr.suffixes[5] , "to be" , ".plugin"   );
      expect(mgr.suffixes[6] , "to be" , ".js"       );

    });

    it("should allow for plugin suffixes as needed", function () {
      var mgr = new PluginManager();
      mgr.addPath(__dirname, "..","tmp","plugins");
      mgr.addPath(__dirname, "..","tmp","plugins","more-plugins");
      mgr.addSuffix('.plugin.js');
    });

    it("should multiplex paths and suffixes on names in order", function () {
      var mgr = new PluginManager();
      mgr.paths.shift();
      mgr.paths.shift();
      mgr.paths.shift();
      mgr.addPath("/tmp","more-plugins");
      mgr.addPath("/tmp","plugins");
      mgr.suffixes.shift();
      mgr.suffixes.shift();
      mgr.suffixes.shift();
      mgr.addSuffix('.plugin');
      mgr.addSuffix('.plugin.js');

      var priorities = mgr.fileplex('my-plugin');

      expect(priorities[0], "to be", "/tmp/plugins/my-plugin.plugin.js"     );
      expect(priorities[1], "to be", "/tmp/plugins/my-plugin.plugin"        );
      expect(priorities[2], "to be", "/tmp/more-plugins/my-plugin.plugin.js");
      expect(priorities[3], "to be", "/tmp/more-plugins/my-plugin.plugin"   );
    });

  });

});
