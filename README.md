# Overview

The `node-plugin-manager` module provides a plugin manager for node that
provides *complimentary* functionality to Node's built-in `require()`
function.

For typical module loading cases, you should simply use the `require()`
function.  In some cases, when different functionality is required, it may
be appropriate to use this module.

# Installation

Install normally through a package manager:

    npm install node-plugin-manager

or

    yarn add node-plugin-manager

# How it Works

When you require the module you get the `PluginManager` class.  When you
instantiate this class, you get a configurable plugin manager that can be
set to search for kinds of plugins and in various locations.

# Usage via Tutorial

Learn by example!  Here's a brief tutorial that you can follow step-by-step
to use the module.  Make sure to read the code comments!

## 1. Create directories

For this tutorial, let's create a simple directory structure:

    $ mkdir plugin-tutorial
    $ cd plugin-tutorial

And our plugin directory

    $ mkdir my-plugins

## 2. Create a plugin

For the sake of our example, let's create a *very* simple plugin.  We'll put
it in the current directory.

## Create a plugin manager

    // Require the plugin to get the PluginManager class.
    const PluginManager = require('node-plugin-manager');

    // Create an instance of the PluginManager.
    var plugins = new PluginManager();

## Adding Paths

    // Let the plugin manager know where to find files.  This defaults to:
    //
    //   ./
    //   ./plugins
    //
    plugins.addPath('./our-plugins');
    plugins.addPath('./my-plugins');

Now that we've added the two additional paths, the searchable paths
array will look like this:

    > plugins.paths
    [
      './my-plugins',
      './our-plugins',
      './',
      './plugins'
    ]

Note that the plugin paths are prioritized in the reverse order in which
they are added.



Now we can load plugins just as we do with `require()`:


# Anatomy of a Plugin

Plugins are just node modules.  Any module can be loaded as a plugin.
There's no difference.

If you would like to add some structure to your plugins you may write your
own PluginManager plugin, or use the `node-plugin` plugin which provides
some core structure for you.

